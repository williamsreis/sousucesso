@extends('layouts.site')
@section('content')
    <header>
      <div class="bg">
        <img src="{{ asset('img/bg-topo.jpg') }}" class="img-fluid mx-auto d-block">
      </div>
     @include('layouts.nav')
      <div class="container tx">
        <div class="row">
          <div class="col-6">
            <h1 class="h1">Marketing de Rede<br> Rápido e sem burocracia</h1>
            <p>O Grupo SOUSUCESSO nasce com a missão de ajudar pessoas a crescer e empreender no Marketing de Rede, acreditamos e queremos que todos tenham a oportunidade de fazer sucesso utilizando as vantagens dessa ferramenta que tem transformados milhares de vidas no Brasil e no Mundo.</p>
          </div>
        </div>
      </div>
    </header>
    <section id="quem-somos" class="container bg-white">
      <div class="row">
        <div class="col-12 mb-5">
          <h2 class="text-center title">MARKETING DE REDE</h2>
        </div>

        <div class="col-4">
            <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid mx-auto d-block mb-4">
            <h3 class="h3 text-center">Risco Zero</h3>
            <p class="texto text-center">O seu investimento inicial de imediato já é revertido em produtos para seu consumo ou venda.</p>
        </div>
        <div class="col-4">
            <img src="{{ asset('img/quemsomos2.png') }}" class="img-fluid mx-auto d-block mb-4">
            <h3 class="h3 text-center">Sem custo fixo</h3>
            <p class="texto text-center">No marketing de rede não existem custos de aluguel, luz, água, telefone, encargos trabalhistas, empregados, etc.</p>
        </div>
        <div class="col-4">
            <img src="{{ asset('img/quemsomos3.png') }}" class="img-fluid mx-auto d-block mb-4">
            <h3 class="h3 text-center">Potencial de ganhos ilimitados</h3>
            <p class="texto text-center">No marketing de rede você pode fazer uma meta de quanto e quando atingir seus ganhos e alcança-los com seu trabalho pessoal e da equipe.</p>
        </div>

      </div>
     </section>
     <section class="container video">
      <div class="row">
        <div class="col">
            <iframe width="100%" height="600" src="https://www.youtube.com/embed/vLuFIhoAX-0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
     </section>
     <section class="vantagens row">
        <div class="col-4">

        </div>
        <div class="col-1 p-4">
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <img src="{{ asset('img/seta-l.png')}}">
            <span class="sr-only">Voltar</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <img src="{{ asset('img/seta-r.png')}}">
            <span class="sr-only">Avançar</span>
          </a>
        </div>
        <div class="col-7">
          <div class="col-sm-10">
        <div id="carouselExampleIndicators" class="carousel mt-8 mb-8 slide carousel-fade" data-ride="carousel">

          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="carousel-caption d-md-block">
                <h2 class="h1">MONTE SEU NEGÓCIO MESMO TENDO UM EMPREGO OU NEGÓCIO TRADICIONAL</h2>
                <p>O marketing de rede pode ser um plano B, você diversifica sua renda trabalhando algumas horas por dia e ainda pode manter seu emprego ou outro negócio.</p>
                <p>
                  <a href="{{ url('conheca-o-plano') }}" class="btn btn-saiba">saiba mais</a>
                </p>
              </div>
            </div>
            <div class="carousel-item">
              <div class="carousel-caption d-md-block">
                <h2 class="h1">PODE TRABALHAR EM CASA</h2>
                <p>Esse é um grande desejo da maioria das pessoas. Essa é uma das grandes vantagens desse negócio. Sua casa pode ser o seu local de trabalho, usando a internet ou fazendo reuniões presenciais em casa ou outros locais.</p>
                <p>
                  <a href="{{ url('conheca-o-plano') }}" class="btn btn-saiba">saiba mais</a>
                </p>
              </div>
            </div>

            <div class="carousel-item">
                <div class="carousel-caption d-md-block">
                  <h2 class="h1">NÃO EXIGE EXPERIENCIA EM NEGÓCIOS</h2>
                  <p>No marketing de rede as pessoas são ensinadas e treinadas para desenvolver o negócio e aprenderem para ensinar e duplicar. </p>
                  <p>
                    <a href="{{ url('conheca-o-plano') }}" class="btn btn-saiba">saiba mais</a>
                  </p>
                </div>
              </div>

          </div>

        </div>
        </div>
        </div>
     </section>
     <section class="parceiros container">

        <div class="row">
              <div class="col-12">
                <h2 class="text-center title">Cases de Sucessos</h2>
              </div>

              <div class="col-12">
                <div class="slideshow "
                  data-cycle-fx="carousel"
                  data-cycle-timeout="0"
                  data-cycle-carousel-visible=4
                  data-cycle-next="#next"
                  data-cycle-prev="#prev"
                  data-cycle-slides="> div"
                  data-carousel-fluid=true
                  >
                  @for($i = 0; $i<count($cases);$i++)
                    @if(isset($cases[$i]->imagem))
                    <div class="col-3">
                      <img src="{{ asset('imagens/retangulo/'.$cases[$i]->imagem->imagem) }}" >
                    </div>
                    @endif
                  @endfor
                  <a href="#" id="prev"><img src="{{ asset('img/seta-l.png')}}"></a>
                  <a href="#" id="next"><img src="{{ asset('img/seta-r.png')}}"></a>
              </div>
            </div>

           </div>





     </section>

 @endsection
