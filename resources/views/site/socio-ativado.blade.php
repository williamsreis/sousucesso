@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page">
		<div class="container">
			<div class="row justify-content-center">
				Parabéns, já estamos trabalhando na sua ativação, em breve você lh enviaremos seu ID e sua senha no seu email, fique atento.
			</div>
		</div>
	</section>
@endsection