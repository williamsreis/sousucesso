@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page mb-8">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                    <h2 class="title text-warning text-center mb-5">Vantagens</h2>
				</div>
                <div class="col-sm-12 mb-8">
					<div class="media">
                        <img src="{{ asset('img/conheca-o-plano.fw.png') }}" class="align-self-start mr-3" alt="Conheça o Plano">
                        <div class="media-body">

						  <p class="texto">Fazer parte do Marketing de Rede já é bom, imagina se associar a um grupo focado em desenvolver esse negócio com seus associados.</p>
                          <p class="texto">Vêm com o Grupo Sou Sucesso fazer parte da maior empresa de Marketing de Rede d Brasil, o GRUPO HINODE.</p>
                          <p class="texto">Quer saber o que tem a mais aqui pra você crescer, confere aqui.</p>
						</div>

					</div>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Atendimento permanente</h3>
                    <p class="texto text-center">dedicamos uma equipe pronta para lhe atende e lhe ajudar a crescer.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos2.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Apresentação do Plano</h3>
                    <p class="texto text-center">Semanalmente nossa equipe faz uma apresentação do plano de negócio para nossos associados e seus convidados.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos3.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Treinamento exclusivo</h3>
                    <p class="texto text-center">Toda semana realizamos treinamentos para nossos associados e convidados.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Cases de sucesso</h3>
                    <p class="texto text-center">Trabalhamos incansavelmente para transformar você no nosso próximo caso de sucesso.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos2.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Técnicas de venda</h3>
                    <p class="texto text-center">Aprimore as suas técnicas de venda e aumente seus lucros vendendo os produtos de excelente qualidade.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos3.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">Apresentação de produtos</h3>
                    <p class="texto text-center">Semanalmente nossos consultores apresentam nossos produtos e seus benefícios, para lhes auxiliar nas vendas.</p>
                </div>

			</div>
		</div>

	</section>
	@include('site.quer-saber-mais')
@endsection
