@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page">
		<div class="container">
			<div class="row justify-content-center">

				<div class="col-12 mb-4">
					<h2 class="title text-warning text-center">Seja nosso sócio</h2>
				</div>
				<div class="col-10 mb-4">
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					{!! Form::open(['route' => 'seja-nosso-socio', 'class'=>'row justify-content-center']) !!}
					<div class="col-8">
                    <div class="row">
                        <div class="text-center d-block justify-content-center mx-auto">
                        <div class="toggle">
                            <input type="radio" name="cadastro" value="pre" id="sizeWeight" checked="checked" />
                            <label for="sizeWeight">Quero fazer um pré cadastro</label>
                            <input type="radio" name="cadastro" value="socio" id="sizeDimensions" />
                            <label for="sizeDimensions">Quero ser um sócio</label>
                        </div>
                        </div>
                        <div class="form-group col-md-6 completo">
                            {{ Form::text('cpf',null,['class' => 'form-control form-control-lg cpf','placeholder' => 'CPF']) }}
                        </div>
                        <div class="form-group col-md-6 completo">
                            {{ Form::text('rg',null,['class' => 'form-control form-control-lg','placeholder' => 'RG']) }}
                        </div>
                    </div>
                    <div class="form-group">
						{{ Form::text('nome',null,['class' => 'form-control form-control-lg','placeholder' => 'Nome']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::email('email',null,['class' => 'form-control form-control-lg','placeholder' => 'E-mail']) }}
                    </div>

                    <div class="row completo">
                        <div class="form-group col-md-6">
                            {{ Form::text('nascimento',null,['class' => 'form-control form-control-lg nascimento','placeholder' => 'Data de Nascimento']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::select('estado_civil',$estadoCivil,null,['class' => 'form-control form-control-lg','placeholder' => 'Estado Civil']) }}
                        </div>
                    </div>
					<div class="form-group">
						{{ Form::text('telefone',null,['class' => 'form-control form-control-lg telefone','placeholder' => 'Telefone']) }}
                    </div>
                    <div class="row completo">
                        <div class="form-group col-md-6">
						    {{ Form::number('dependentes',null,['class' => 'form-control form-control-lg','placeholder' => 'Dependentes']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::text('patrocinador',null,['class' => 'form-control form-control-lg','placeholder' => 'Identificação do Patrocinador']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            {{ Form::text('cep',null,['class' => 'form-control form-control-lg cep','placeholder' => 'CEP', 'id'=>'cep']) }}
                        </div>
                        <div class="form-group col-md-6 mt-2">
                            <a class="btn btn-link" href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm" target="_blank">
                                Não sei meu cep
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-9">
                            {{ Form::text('endereco',null,['class' => 'form-control form-control-lg','placeholder' => 'Endereço', 'id'=>'rua']) }}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::text('numero',null,['class' => 'form-control form-control-lg','placeholder' => 'Número']) }}
                        </div>
                    </div>

					<div class="row">
                        <div class="form-group col-md-6">
                            {{ Form::text('bairro',null,['class' => 'form-control form-control-lg','placeholder' => 'Bairro','id'=>'bairro']) }}
                        </div>
                        <div class="form-group col-md-6">
						    {{ Form::text('cidade',null,['class' => 'form-control form-control-lg cidade','placeholder' => 'Cidade/UF','id'=>'cidade']) }}
                        </div>
					</div>
                    <div class="form-group">
                        {{ Form::text('complemento',null,['class' => 'form-control form-control-lg','placeholder' => 'Complemento']) }}
                    </div>

					<div class="form-group">
						{{ Form::textarea('obj',null,['class' => 'form-control form-control-lg','placeholder' => 'Observações']) }}
					</div>
					</div>
					<div class="col-12 mb-4">
					<h2 class="title text-warning text-center">Escolha seu plano</h2>
				</div>
				<section class="radioStyle col-12">
					@foreach($planos as $plano)
					<div class="pr-3">
					  <input type="radio" id="plano_{{ $plano->id }}" name="plano_id" value="{{ $plano->id }}">
					  <label for="plano_{{ $plano->id }}">
					    <h2>{{ $plano->valorReal }}</h2>
					    <p>{!! $plano->descricao !!}</p>
					  </label>
					</div>
					@endforeach
				</section>
				<div class="col-12 mb-4 mt-4 text-center">
					<button type="submit" class="btn">
						<img class="img-fluid" src="{{ asset('img/btn-quero-ser-socio.png')}}">
					</button>
				</div>
				{!! Form::close() !!}

				</div>

			</div>
		</div>
	</section>
@endsection
