@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page mb-8">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="title text-warning text-center mb-5">Cases de Sucesso</h2>
				</div>
				@if(isset($cases[0]))
				<div class="col-6 offset-3">
					<div id="carouselCase" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner">
					    @php
					    $i = 0;
					    @endphp
					    @foreach($cases[0]->imagens as $imagem)
					    <div class="carousel-item @if($i++ == 0) active @endif">
					      <img src="{{ asset('imagens/quadrado400/'.$imagem->imagem) }}" class=" mx-auto d-block rounded-circle" alt="{{ $cases[0]->nome }}">
					    </div>
					    @endforeach
					  </div>
					  <a class="carousel-control-prev" href="#carouselCase" role="button" data-slide="prev">
					    <img src="{{ asset('img/seta-l.png')}}">
					    <span class="sr-only">Voltar</span>
					  </a>
					  <a class="carousel-control-next" href="#carouselCase" role="button" data-slide="next">
					    <img src="{{ asset('img/seta-r.png')}}">
					    <span class="sr-only">Avançar</span>
					  </a>
					</div>
				</div>
				<div class="col-10 offset-1 text-center case black4">
					{!! $cases[0]->descricao !!}
					<h3 class="orange">{{ $cases[0]->nome }}</h3>
					<h5 class="black4">{{ $cases[0]->empresa }}</h5>
				</div>
				
				@endif
			</div>
		</div>
		@for($i=1; $i<count($cases); $i++)
		@php
			if($i%2 == 0){
				$classe = 'b-orange';
			}else{
				$classe = 'b-black';
			}
		@endphp
			<section class="w-100 case b-black {{ $classe}}">
				<div class="container">
					<div class="row">
						@if(isset($cases[$i]->imagem))
						<div class="col-3">
							<img class="img-fluid mx-auto d-block rounded-circle" src="{{ asset('imagens/quadrado250/'.$cases[$i]->imagem->imagem) }}">
						</div>
						@endif
						<div class="col">
							{!! $cases[$i]->descricao !!}

							<h3>{{ $cases[$i]->nome }}</h3>
							<h5>{{ $cases[$i]->empresa }}</h5>
						</div>
					</div>
				</div>
			</section>
		@endfor

		@if($cases->total() > 20)
		<div class="container mt-4">
			<div class="row">
				<div class="col-12 mx-auto d-block">
					{{ $cases->render()}}
				</div>
			</div>
		</div>
		@endif

		@include('site.quer-saber-mais')

	</section>
@endsection