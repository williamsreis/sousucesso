@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page">
		<div class="container">
			<div class="row justify-content-center">
				Bem vindo(a) ao Grupo Sou Sucesso, nossa equipe lhe enviou um email para confirmação do seu interesse e cadastro, confira lá.
			</div>
		</div>
	</section>
@endsection