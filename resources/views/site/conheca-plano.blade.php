@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page mb-8">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
                    	<h2 class="title text-warning text-center mb-5">Conheça o plano</h2>
				</div>
				<div class="col-sm-12 mb-8">
					<div class="media">
						<div class="media-body">
						  <h5 class="mt-0 mb-1">MARKETING DE REDE, UM NEGÓCIO ONDE TODOS GANHAM</h5>
						  <p class="texto">Além de todas as vantagens do MR, ainda tem a garantia a suporte da maior Empresa do Marketing de Rede da América Latina, o Grupo HINODE, Empresa 100% Brasileira, com um portfólio com mais de 800 produtos, com fábrica e distribuição própria, possui franquias em todos os Estados e principais cidades. Ainda tem dúvida se vale a pena entrar no Marketing de Rede? Navegue no nosso site e faça um pré cadastro que lhe faremos uma apresentação personalizada.</p>
						</div>
						<img src="{{ asset('img/conheca-o-plano.fw.png') }}" class="ml-3" alt="Conheça o Plano">
					</div>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">TER SEU TEMPO LIVRE</h3>
                    <p class="texto text-center">Você pode acordar na hora que quiser, trabalhar o dia que quiser e no horário mais conveniente e quantas horas achar necessário por dia.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos2.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">NÃO EXIGE EXPERIENCIA EM NEGÓCIOS</h3>
                    <p class="texto text-center">No marketing de rede as pessoas são ensinadas e treinadas para desenvolver o negócio e aprenderem para ensinar e duplicar.</p>
                </div>
                <div class="col-4">
                    <img src="{{ asset('img/quemsomos3.png') }}" class="img-fluid mx-auto d-block mb-4">
                    <h3 class="h3 text-center">TER AJUDA DE PESSOAS ESPECIALIZADAS</h3>
                    <p class="texto text-center">Você terá ajuda e orientação gratuita da sua linha ascendente, pessoas especializadas, professores que vão lhe ensinar tudo o que sabem, visando seu crescimento.</p>
                </div>
			</div>
		</div>

	</section>
	@include('site.quer-saber-mais')
@endsection
