<div class="container mt-8 mb-8">
	<div class="row">
		<div class="col-sm-12 text-center">
			<h2 class="title text-warning mb-5">Quer saber mais?</h2>
			<p class="mb-5">Entre em contato por um dos nosso canais de atendimento, para lhe mostrarmos casos de sucesso, pessoas que acreditaram nesse método de empreender e tiveram suas vidas e de suas famílias transformadas.</p>
			<a href="{{ url('/seja-nosso-socio') }}">
				<img src="{{ asset('img/btn-quero-saber-mais.png') }}">
			</a>
		</div>

	</div>
</div>
