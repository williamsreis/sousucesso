@extends('layouts.site')
@section('content')
	@include('layouts.nav-black')
	<section class="page mb-8">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="title text-warning text-center mb-5">Vantagens</h2>
				</div>
				<div class="col-md-4">
		            <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid">
		            <h3>Lorem ipsum dolor</h3>
		            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
		        </div>
		        <div class="col-md-4">
		            <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid">
		            <h3>Lorem ipsum dolor</h3>
		            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
		        </div>
		        <div class="col-md-4">
		            <img src="{{ asset('img/quemsomos.png') }}" class="img-fluid">
		            <h3>Lorem ipsum dolor</h3>
		            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
		        </div>
			</div>
		</div>

	</section>
	@include('site.quer-saber-mais')
@endsection