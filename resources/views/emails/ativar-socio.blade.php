<p>Olá <b>{{ $socio->nome }}</b>,</p>
<p>Você está a um passo de entrar no <b>Marketing em Rede</b>, seja bem-vindo ao mercado que já transformou milhares de vidas mundo a fora, só confirma para a gente seu interesse em ingressar nesse mercado, que lhe daremos todo o suporte e instruções necessárias.</p>
<p>&nbsp;</p>
<a href="{{ url('quero-entrar-no-marketing-em-rede/'.$socio->id) }}">QUERO ENTRAR NO MARKETING EM REDE</a>
<p>&nbsp;</p>
<p>Caso não tenha feito o pré-cadastro, ou ainda não pretende ingressar nesse mercado transformador, desconsidere esta mensagem.</p>

