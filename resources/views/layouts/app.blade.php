<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/painel.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                        @if(Auth::user()->is_patrocinador == 1)
                        <li class="nav-item @if(Request::is('painel/cases*')) active @endif">
                            <a class="nav-link" href="{{ route('cases.index') }}"><i class="fas fa-suitcase"></i> Cases de Sucesso</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/planos*')) active @endif">
                            <a class="nav-link" href="{{ route('planos.index') }}"><i class="fas fa-file-invoice-dollar"></i> Planos</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/vantage*')) active @endif">
                            <a class="nav-link" href="{{ route('vantagens.index') }}"><i class="fas fa-tasks"></i> Vantagens</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/produtos*')) active @endif">
                            <a class="nav-link" href="{{ route('produtos.index') }}"><i class="fab fa-product-hunt"></i> Produtos</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/socios*')) active @endif">
                            <a class="nav-link" href="{{ route('socios.index') }}"><i class="fas fa-user-tie"></i> Sócios</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/solicitacoes*')) active @endif">
                            <a class="nav-link" href="{{ route('solicitacoes.index') }}"><i class="fab fa-slideshare"></i> Solicitaões de Apresentação</a>
                        </li>
                        @else
                        <li class="nav-item @if(Request::is('painel/produtos*')) active @endif">
                            <a class="nav-link" href="{{ route('produtos.index') }}"><i class="fab fa-product-hunt"></i> Produtos</a>
                        </li>
                        <li class="nav-item @if(Request::is('painel/solicitacoes*')) active @endif">
                            <a class="nav-link" href="{{ route('solicitacoes.create') }}"><i class="fab fa-slideshare"></i> Solicitar Apresentação</a>
                        </li>
                        @endif
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>

                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fas fa-sign-out-alt"></i> {{ __('Sair') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    @yield('script')
    <script src="{{ asset('js/painel.js') }}"></script>
</body>
</html>
