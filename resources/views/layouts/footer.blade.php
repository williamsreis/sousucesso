<section class="footer">
      <div class="container">
      <div class="row">
        <div class="col-6">
          <h2 class="title">Quero mais informações</h2>
          <p>
          <img width="24" height="24" src="{{ asset('img/whatsapp-brands.svg')}}"/>
          (93) 99191-3691
          </p>
          <div class="row">
          <form class="col-8" id="formContato" method="post" action="{{ route('faleconosco')}}">
            @csrf
            <div class="form-group">
              <input type="text" placeholder="Nome" name="nome" class="form-control form-control-lg ">
            </div>
            <div class="form-group">
              <input type="email" placeholder="E-mail" name="email" class="form-control form-control-lg">
            </div>
            <div class="form-group">
              <textarea name="mensagem" placeholder="Mensagem" class="form-control form-control-lg"></textarea>
            </div>
            <button type="submit">
              <img src="{{ asset('img/btn-enviar.png')}}">
            </button>
          </form>
          </div>
        </div>
        <div class="col-6 text-right pt-8 text-white">
          <p class="text-warning">SIGA-NOS
          <a href="https://www.facebook.com/sousucessobr/?modal=admin_todo_tour" target="_blank">
            <img src="{{ asset('img/facebook.png')}}">
          </a>
          <img src="{{ asset('img/instagram.png')}}">
          </p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>Consultor independente associada ao grupo <img src="{{ asset('img/hinode.png')}}"></p>
          <p>Venha fazer parte do grupo Sou Sucesso<br> e crescer com a gente no Marketing de Rede.</p>
          <p>
            <a href="http://www.w3mais.com" target="_blank">
                <img src="{{ asset('img/w3mais.png')}}">
            </a>
          </p>

        </div>
      </div>
      </div>
     </section>
