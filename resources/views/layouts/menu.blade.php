<div class="container">


        <a class="navbar-brand" href="{{ url('/')}}" title="Sou Sucesso">
          <img src="{{ asset('img/logomarca.png') }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse float-right text-right" id="navbarSupportedContent">
          <ul class="navbar-nav float-right text-right">
            <li class="nav-item @if(Request::is('conheca-o-plano')) active @endif">
              <a class="nav-link" href="{{ url('conheca-o-plano') }}">Conheça o plano</a>
            </li>
            <li class="nav-item @if(Request::is('vantagens')) active @endif">
              <a class="nav-link" href="{{ url('vantagens')}}">Vantagens</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://www.hinodeonline.com.br/?id_consultor=22886156" target="_blank">Produtos</a>
            </li>

            <li class="nav-item @if(Request::is('cases-de-sucesso')) active @endif">
              <a class="nav-link" href="{{ url('cases-de-sucesso')}}">Cases de sucesso</a>
            </li>
            <li class="nav-item  @if(Request::is('seja-nosso-socio')) active @endif">
              <a class="nav-link" href="{{ url('seja-nosso-socio') }}">Seja nosso sócio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn btn-warning" href="{{ url('virtual-office/login')}}">Virtual office</a>
            </li>
          </ul>

        </div>
        </div>
