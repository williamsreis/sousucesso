@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Produtos'))

@section('content')
<div class="container">
<div class="card box-primary">
  <div class="card-header with-border">
    <h3 class="box-title">Editar Produtos</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::model($case,['role' => 'form', 'method' => 'PUT', 'route' => ['produtos.update',$case->id],'files' => true])}}
    
    @include('painel.produtos.form')

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Editar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection