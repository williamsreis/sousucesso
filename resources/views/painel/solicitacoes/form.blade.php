<div class="card-body">
  @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
  <input name="socio_id" value="{{ Auth::user()->socio->id }}" type="hidden">
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="1" name="local_definido" id="defaultCheck1">
      <label class="form-check-label" for="defaultCheck1">
        Tem local definido?
      </label>
    </div>  
  </div>  
  <div class="form-group{{ $errors->has('qtd_pessoas') ? ' has-error' : '' }}">
    {{ Form::label('Quantidade de Pessoas *') }}
    {{ Form::number('qtd_pessoas',null, ['class' => 'form-control']) }}
    @if ($errors->has('qtd_pessoas'))
    <span class="help-block">
      {{ $errors->first('qtd_pessoas') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
    {{ Form::label('Data *') }}
    {{ Form::text('data',null, ['class' => 'form-control data']) }}
    @if ($errors->has('data'))
    <span class="help-block">
      {{ $errors->first('data') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('horario') ? ' has-error' : '' }}">
    {{ Form::label('Horário *') }}
    {{ Form::text('horario',null, ['class' => 'form-control horario']) }}
    @if ($errors->has('horario'))
    <span class="help-block">
      {{ $errors->first('horario') }}
    </span>
    @endif
  </div>
  
  <div class="form-group{{ $errors->has('obs') ? ' has-error' : '' }}">
    {{ Form::label('Observação *') }}
    {{ Form::textarea('obs',null, ['class' => 'form-control']) }}
    @if ($errors->has('obs'))
    <span class="help-block">
      {{ $errors->first('obs') }}
    </span>
    @endif
  </div>

</div>