@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Solicitações de Apresentação'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
              <h2 class="">Solicitações de Apresentação</h2>
        </div>
			
      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {{ session('message') }}
                  </div>
              @endif
              <table class="table table-bordered table-hover">
                <tr>
                  <th>ID</th>
                  					
                  <th>Sócio</th>                 
						      <th>Quantidade de Pessoas</th>
                  <th>Data</th>
                  <th>Horário</th>
                  <th>Local Definido?</th>
                  <th>Observações</th>
						      <th class="text-center" width="10%">Ações</th>
                </tr>
                @foreach($solicitacoes as $solicitacao)
                <tr>
                  <td width="3%">                    
                    {{ $solicitacao->id }}
                  </td>	                 
						      <td>{{ $solicitacao->socio->nome }}</td>
						      <td>{!! $solicitacao->qtd_pessoas !!}</td>
                  <td>{!! $solicitacao->data->format('d/m/Y') !!}</td>
                  <td>{!! $solicitacao->horario !!}</td>
                  <td>{!! $solicitacao->local_definido !!}</td>
                  <td>{!! $solicitacao->obs !!}</td>
                  <td> 
                    {!! Form::open(['route' => ['solicitacoes.destroy',$solicitacao->id], 'method' => 'DELETE']) !!}
                      <button class="btn btn-danger" type="submit" title="Remover"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    {!! Form::close() !!}
                    
                  </td>
                  
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $solicitacoes->render() }}
            </div>
          </div>
          <!-- /.box -->
		</div>	
	</div>
</div>
@stop
