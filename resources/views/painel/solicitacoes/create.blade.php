@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Solicitar Apresentação'))

@section('content')
<div class="container">
  <div class="row">
<div class="col-6 offset-3">
<div class="card card-primary">
  <div class="card-header">
      <h2>Solicitar Apresentação</h2>
  </div>
    

  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::open(['role' => 'form', 'method' => 'POST', 'route' => 'solicitacoes.store', 'files' => true])}}
    @include('painel.solicitacoes.form')
   	<div class="card-footer">
      <button type="submit" class="btn btn-primary">Solicitar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
</div>
</div>
@endsection