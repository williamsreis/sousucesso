@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Planos'))

@section('content')
<div class="container">
<div class="card box-primary">
  <div class="card-header with-border">
    <h3 class="box-title">Editar Planos</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::model($case,['role' => 'form', 'method' => 'PUT', 'route' => ['planos.update',$case->id],'files' => true])}}
    
    @include('painel.planos.form')

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Editar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection