@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Vantagens'))

@section('content')
<div class="container">
<div class="card box-primary">
  <div class="card-header with-border">
    <h3 class="box-title">Editar Vantagens</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::model($vantagem,['role' => 'form', 'method' => 'PUT', 'route' => ['vantagens.update',$vantagem->id],'files' => true])}}
    
    @include('painel.vantagens.form')

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Editar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection