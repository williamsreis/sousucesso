@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Vantagens'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
            <div class="row">
             <div class="col-6">
              <h2 class="">Vantagens</h2>
             </div>
             <div class="col-6">
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a href="{{ route('vantagens.create')}}" class="nav-link btn btn-primary">Cadastrar</a>
                </li>
              </ul>
             </div>
            </div>  
             
            </div>
			
      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {{ session('message') }}
                  </div>
              @endif
              <table class="table table-bordered table-hover">
                <tr>
                  <th>
                    <input type="checkbox" name="selecionarTodos" title="Selecionar Todos" class="select-input-all">
                  </th>
                  					
                  <th>Nome</th>                 
						      <th>Imagem</th>
						      <th colspan="2" class="text-center" width="10%">Ações</th>
                </tr>
                @foreach($vantagens as $vantagem)
                <tr>
                  <td width="3%">                    
                    <input class="arrayid" type="checkbox" name="id" value="{{ $vantagem->id }}">
                  </td>	
                 
						      <td>{{ $vantagem->titulo }}</td>
						      <td>
                    <img src="{{ asset($vantagem->imagem) }}"/>
                  </td>
                  <td class="text-center">
                    
                    <a class="btn btn-warning" href="{{ route('vantagens.edit', $vantagem->id )}}" title="Editar"><i class="far fa-edit"></i></a>
                    
                  </td>
                  <td class="text-center"> 
                    {!! Form::open(['route' => ['vantagens.destroy',$vantagem->id], 'method' => 'DELETE']) !!}
                      <button class="btn btn-danger" type="submit" title="Remover"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    {!! Form::close() !!}
                    
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $vantagens->render() }}
            </div>
          </div>
          <!-- /.box -->
		</div>	
	</div>
</div>
@stop
