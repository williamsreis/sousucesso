<div class="card-body">
  
  <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
    {{ Form::label('Título *') }}
    {{ Form::text('titulo',null, ['class' => 'form-control']) }}
    @if ($errors->has('titulo'))
    <span class="help-block">
      {{ $errors->first('titulo') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
    {{ Form::label('URL') }}
    {{ Form::url('url',null, ['class' => 'form-control']) }}
    @if ($errors->has('url'))
    <span class="help-block">
      {{ $errors->first('url') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('imagem') ? ' has-error' : '' }}">
    {{ Form::label('Imagem *') }}
    {{ Form::file('imagem', ['class' => 'form-control-file']) }}
    @if ($errors->has('imagem'))
    <span class="help-block">
      {{ $errors->first('imagem') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
    {{ Form::label('Descrição *') }}
    {{ Form::textarea('descricao',null, ['class' => 'form-control ckeditor']) }}
    @if ($errors->has('descricao'))
    <span class="help-block">
      {{ $errors->first('descricao') }}
    </span>
    @endif
  </div>
</div>
@section('script')
  @parent
  <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection