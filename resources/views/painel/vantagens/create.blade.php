@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Vantagens'))

@section('content')
<div class="container">
<div class="card card-primary">
  <div class="card-header">
    <div class="row">
      <div class="col-6">
        <h2>Cadastrar</h2>
      </div>
      <div class="col-6">
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <a href="{{ route('vantagens.index') }}" class="nav-link btn btn-primary">Listar Vantagens</a>
          </li>
        </ul>
      </div>
    </div>
    
    
  </div>

  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::open(['role' => 'form', 'method' => 'POST', 'route' => 'vantagens.store', 'files' => true])}}
    @include('painel.vantagens.form')
   	<div class="card-footer">
      <button type="submit" class="btn btn-primary">Cadastrar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection