@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Case de Sucesso'))

@section('content')
<div class="container">
<div class="card box-primary">
  <div class="card-header with-border">
    <h3 class="box-title">Editar Case de Sucesso</h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::model($case,['role' => 'form', 'method' => 'PUT', 'route' => ['cases.update',$case->id],'files' => true])}}
    
    @include('painel.cases.form')

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Editar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection