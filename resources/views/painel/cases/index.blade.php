@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Cases de Sucesso'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
            <div class="row">
             <div class="col-6">
              <h2 class="">Cases de Sucesso</h2>
             </div>
             <div class="col-6">
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a href="{{ route('cases.create')}}" class="nav-link btn btn-primary">Cadastrar</a>
                </li>
              </ul>
             </div>
            </div>  
             
            </div>
			
      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {{ session('message') }}
                  </div>
              @endif
              <table class="table table-bordered table-hover">
                <tr>
                  <th>
                    <input type="checkbox" name="selecionarTodos" title="Selecionar Todos" class="select-input-all">
                  </th>
                  					
                  <th>Nome</th>                 
						      <th>Empresa</th>
                  <th>Imagens</th>
						      <th colspan="2" class="text-center" width="10%">Ações</th>
                </tr>
                @foreach($cases as $case)
                <tr>
                  <td width="3%">                    
                    <input class="arrayid" type="checkbox" name="id" value="{{ $case->id }}">
                  </td>	
                 
						      <td>{{ $case->nome }}</td>
						      <td>{{ $case->empresa }}</td>
                  <td>
                    <a href="{{ route('imagens.create',$case->id) }}" class="btn btn-primary"><i class="fas fa-plus-square"></i></a>
                    <a href="{{ route('imagens.index',$case->id) }}" class="btn btn-primary">
                      <i class="fas fa-eye"></i> {{ $case->imagens()->count() }}
                    </a>
                  </td>
                  
                  <td class="text-center">
                    
                    <a class="btn btn-warning" href="{{ route('cases.edit', $case->id )}}" title="Editar"><i class="far fa-edit"></i></a>
                    
                  </td class="text-center">
                  <td> 
                    {!! Form::open(['route' => ['cases.destroy',$case->id], 'method' => 'DELETE']) !!}
                      <button class="btn btn-danger" type="submit" title="Remover"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    {!! Form::close() !!}
                    
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $cases->render() }}
            </div>
          </div>
          <!-- /.box -->
		</div>	
	</div>
</div>
@stop
