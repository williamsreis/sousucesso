<div class="card-body">
  
  <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
    {{ Form::label('Nome *') }}
    {{ Form::text('nome',null, ['class' => 'form-control']) }}
    @if ($errors->has('nome'))
    <span class="help-block">
      {{ $errors->first('nome') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('empresa') ? ' has-error' : '' }}">
    {{ Form::label('Empresa') }}
    {{ Form::text('empresa',null, ['class' => 'form-control']) }}
    @if ($errors->has('empresa'))
    <span class="help-block">
      {{ $errors->first('empresa') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
    {{ Form::label('Descrição *') }}
    {{ Form::textarea('descricao',null, ['class' => 'form-control ckeditor']) }}
    @if ($errors->has('descricao'))
    <span class="help-block">
      {{ $errors->first('descricao') }}
    </span>
    @endif
  </div>
</div>
@section('script')
  @parent
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection