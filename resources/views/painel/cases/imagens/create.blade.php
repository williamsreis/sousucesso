@extends('layouts.app')
  
@section('title', (isset($titlePage)? $titlePage : 'Case de Sucesso: '.$case->nome.' (Imagens)'))

@section('content')
<div class="container">
<div class="card card-primary">
  <div class="card-header">
    <div class="row">
      <div class="col-8">
        <h2>Case de Sucesso: {{ $case->nome }} (Imagens)</h2>
      </div>
      <div class="col-4">
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <a href="{{ route('imagens.index',$case->id) }}" class="nav-link btn btn-primary">Listar Imagens</a>
          </li>
        </ul>
      </div>
    </div>
    
    
  </div>

  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::open(['class' => 'dropzone','role' => 'form', 'method' => 'POST', 'route' => ['imagens.store',$case->id], 'files' => true])}}
    <div class="fallback">
      <input name="file" type="file" multiple />
    </div>
    <div class="dz-message needsclick">
      Solte os arquivos aqui ou clique para fazer o upload. 
    </div>
  {{ Form::close() }}
</div>
</div>

@endsection
@section('script')
  @parent
<script src="{{ asset('js/dropzone.js') }}"></script>
@endsection