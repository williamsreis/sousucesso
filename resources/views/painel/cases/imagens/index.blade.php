@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Imagens - '.$case->nome))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
            <div class="row">
             <div class="col-6">
              <h2 class="">Imagens - {{ $case->nome }}</h2>
             </div>
             <div class="col-6">
              <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a href="{{ route('imagens.create',$case->id)}}" class="nav-link btn btn-primary">Cadastrar</a>
                </li>
              </ul>
             </div>
            </div>  
             
            </div>
			
      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {{ session('message') }}
                  </div>
              @endif
              <div class="row">

              @foreach($imagens as $imagem)
              <div class="col-2 border mb-2 mr-2">
                <img src="{{ asset('/imagens/small/'.$imagem->imagem) }}" class="img-fluid mx-auto d-block" >
                <p class="m-1 text-center"><a href="#" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Apagar</a></p>
              </div>
              @endforeach
              </div>
            </div>
            
          </div>
          <!-- /.box -->
		</div>	
	</div>
</div>
@stop
