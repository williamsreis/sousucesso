@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Editar Usuário'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Usuário</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('usuario.update',[$socio->id,$usuario->id]) }}">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf

                        <div class="form-group form-check row">
                         
                        <div class="col-md-6 offset-md-4">
                            <input value="1" name="patrocinador" type="checkbox" class="form-check-input" id="exampleCheck1" @if($usuario->is_patrocinador == 1) checked @endif>
                            <label class="form-check-label" for="exampleCheck1">É um patricionador?</label>
                        </div>
                      </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $socio->nome }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">ID Patrocinador</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $usuario->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Endereço Hinode</label>

                            <div class="col-md-6">
                                <input id="url_hinode" type="url" class="form-control{{ $errors->has('url_hinode') ? ' is-invalid' : '' }}" name="url_hinode" value="{{ $socio->url_hinode }}" required>

                                @if ($errors->has('url_hinode'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('url_hinode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Endereço Sou Sucesso</label>

                            <div class="col-md-6">
                                <input id="url_sousucesso" type="url" class="form-control{{ $errors->has('url_sousucesso') ? ' is-invalid' : '' }}" name="url_sousucesso" value="{{ $socio->url_sousucesso }}" required>

                                @if ($errors->has('url_sousucesso'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('url_sousucesso') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cadastrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
