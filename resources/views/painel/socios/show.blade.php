@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Socio'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 mb-2">
			<ul class="nav justify-content-end">

                <li class="nav-item">
                  <a href="{{ route('socios.index')}}" class="nav-link btn btn-primary">Sócios</a>
                </li>

              </ul>
		</div>
		<div class="col-12">
            <h2>{{ $socio->nome }}</h2>
            <table class="table table-bordered">
                <tr>
                    <td>CPF</td>
                    <td>{{ $socio->cpf }}</td>
                    <td>RG</td>
                    <td>{{ $socio->rg }}</td>
                </tr>
                <tr>
                    <td>Nome</td>
                    <td colspan="3">{{ $socio->nome }}</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td colspan="3">{{ $socio->email }}</td>
                </tr>
                <tr>
                    <td>Data de Nascimento</td>
                    <td>{{ $socio->nascimento->format('d/m/Y') }}</td>
                    <td>Estado Civil</td>
                    <td>{{ $socio->estado_civil }}</td>
                </tr>
                <tr>
                    <td>Telefone</td>
                    <td colspan="3">{{ $socio->telefone }}</td>
                </tr>
                <tr>
                    <td>Dependentes</td>
                    <td>{{ $socio->dependentes }}</td>
                    <td>ID Patrocinador</td>
                    <td>{{ $socio->patrocinador }}</td>
                </tr>
                <tr>
                    <td>CEP</td>
                    <td colspan="3">{{ $socio->cep }}</td>
                </tr>
                <tr>
                    <td>Endereço</td>
                    <td>{{ $socio->endereco }}</td>
                    <td>Número</td>
                    <td>{{ $socio->numero }}</td>
                </tr>
                <tr>
                    <td>Bairro</td>
                    <td>{{ $socio->bairro }}</td>
                    <td>Cidade/UF</td>
                    <td>{{ $socio->cidade }}</td>
                </tr>
                <tr>
                    <td>Complemento</td>
                    <td colspan="3">{{ $socio->complemento }}</td>
                </tr>
                <tr>
                    <td>Observações</td>
                    <td colspan="3">{{ $socio->observacoes }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
