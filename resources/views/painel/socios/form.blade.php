<div class="card-body">
  
  <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
    {{ Form::label('Nome *') }}
    {{ Form::text('nome',null, ['class' => 'form-control']) }}
    @if ($errors->has('nome'))
    <span class="help-block">
      {{ $errors->first('nome') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('valor') ? ' has-error' : '' }}">
    {{ Form::label('Valor *') }}
    {{ Form::text('valor',null, ['class' => 'form-control price']) }}
    @if ($errors->has('valor'))
    <span class="help-block">
      {{ $errors->first('valor') }}
    </span>
    @endif
  </div>
  <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
    {{ Form::label('Descrição *') }}
    {{ Form::textarea('descricao',null, ['class' => 'form-control ckeditor']) }}
    @if ($errors->has('descricao'))
    <span class="help-block">
      {{ $errors->first('descricao') }}
    </span>
    @endif
  </div>
</div>
@section('script')
  @parent
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/jquery.priceformat.js') }}"></script>
<script type="text/javascript">
$('.price').priceFormat({
    prefix: 'R$ ',
    centsSeparator: ',',
    thousandsSeparator: '.'
});
</script>
@endsection