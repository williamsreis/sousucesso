@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Sócios'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
            <div class="row">
             <div class="col-6">
              <h2 class="">Sócios</h2>
             </div>
             <div class="col-6">

             </div>
            </div>

            </div>

      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {!! session('message') !!}
                  </div>
              @endif
              <table class="table table-bordered table-hover">
                <tr>
                  <th>
                    <input type="checkbox" name="selecionarTodos" title="Selecionar Todos" class="select-input-all">
                  </th>
                  <th>CPF</th>
                  <th>Nome</th>
				  <th>Telefone</th>
                  <th>E-mail</th>
                  <th>Cidade</th>
                  <th>ID Patrocinador</th>
                  <th>ID usuário</th>
						      <th colspan="3" class="text-center" width="10%">Ações</th>
                </tr>
                @foreach($socios as $socio)
                <tr>
                  <td width="3%">
                    <input class="arrayid" type="checkbox" name="id" value="{{ $socio->id }}">
                  </td>
                  <td>{{ $socio->cpf }}</td>
                  <td>{{ $socio->nome }}</td>
                  <td>{{ $socio->telefone }}</td>
                  <td>{{ $socio->email }}</td>
                  <td>{{ $socio->cidade }}</td>
                  <td>{{ $socio->patrocinador }}</td>
                  <td>{{ $socio->user->email }}</td>
                  <td class="text-center">
                    @if($socio->user->email != "")
                      <a class="btn btn-dark" href="{{ route('usuario.edit', [$socio->id,$socio->user->id] )}}" title="Editar Usuário"><i class="fas fa-user-edit"></i></a>
                    @else
                      <a class="btn btn-dark" href="{{ route('usuario.create', $socio->id )}}" title="Adicionar Usuário"><i class="fas fa-user-plus"></i></a>
                    @endif
                  </td class="text-center">
                  <td class="text-center">

                    <a class="btn btn-info" href="{{ route('socios.show', $socio->id )}}" title="Visualizar"><i class="fas fa-eye"></i></a>

                  </td class="text-center">
                  <td>
                    {!! Form::open(['class'=>'formRemover', 'route' => ['socios.destroy',$socio->id], 'method' => 'DELETE']) !!}
                      <button class="btn btn-danger" type="submit" title="Remover"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    {!! Form::close() !!}

                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $socios->render() }}
            </div>
          </div>
          <!-- /.box -->
		</div>
	</div>
</div>
@stop
