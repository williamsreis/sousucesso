@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Produtos'))

@section('content')
<div class="container">
<div class="card card-primary">
  <div class="card-header">
    <div class="row">
      <div class="col-6">
        <h2>Cadastrar</h2>
      </div>
      <div class="col-6">
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <a href="{{ route('produtos.index') }}" class="nav-link btn btn-primary">Listar Produtos</a>
          </li>
        </ul>
      </div>
    </div>
    
    
  </div>

  <!-- /.box-header -->
  <!-- form start -->
  {{ Form::open(['role' => 'form', 'method' => 'POST', 'route' => 'produtos.store', 'files' => true])}}
    @include('painel.produtos.form')
   	<div class="card-footer">
      <button type="submit" class="btn btn-primary">Cadastrar</button>
    </div>
  {{ Form::close() }}
</div>
</div>
@endsection