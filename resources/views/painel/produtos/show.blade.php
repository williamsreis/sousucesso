@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Produtos'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 mb-2">
			<ul class="nav justify-content-end">

                <li class="nav-item">
                  <a href="{{ route('produtos.index')}}" class="nav-link btn btn-primary">Produtos</a>
                </li>

              </ul>
		</div>
		<div class="col-12">

			<div class="card">
				<div class="card-body p-5">
					<h2>{{ $produto->nome }}</h2>
					{!! $produto->descricao !!}
					<div class="embed-responsive embed-responsive-16by9 mt-5">
						{!! $produto->video !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
