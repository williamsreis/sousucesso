@extends('layouts.app')

@section('title', (isset($titlePage)? $titlePage : 'Produtos'))


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
            <div class="row">
             <div class="col-6">
              <h2 class="">Produtos</h2>
             </div>
             <div class="col-6">
              <ul class="nav justify-content-end">
                @if(Auth::user()->is_patrocinador)
                <li class="nav-item">
                  <a href="{{ route('produtos.create')}}" class="nav-link btn btn-primary">Cadastrar</a>
                </li>
                @endif
              </ul>
             </div>
            </div>  
             
            </div>
			
      		<div class="card-body table-responsive">
              @if (session('message'))
                  <div class="alert alert-success">
                      {{ session('message') }}
                  </div>
              @endif
              <table class="table table-bordered table-hover">
                <tr>
                  <th>ID
                  </th>
                  					
                  <th>Nome</th>                 
						      <th>Descrição</th>
						      <th colspan="3" class="text-center" width="10%">Ações</th>
                </tr>
                @foreach($produtos as $produto)
                <tr>
                  <td width="3%">                    
                    {{ $produto->id }}
                  </td>	
                 
						      <td>{{ $produto->nome }}</td>
						      <td>{!! $produto->descricao !!}</td>
                  @if(Auth::user()->is_patrocinador)
                  <td class="text-center">
                    
                    <a class="btn btn-warning" href="{{ route('produtos.edit', $produto->id )}}" title="Editar"><i class="far fa-edit"></i></a>
                    
                  </td>
                  @endif
                  <td class="text-center">
                    
                    <a class="btn btn-dark" href="{{ route('produtos.show', $produto->id )}}" title="Visualizar"><i class="fas fa-eye"></i></a>
                    
                  </td>
                  @if(Auth::user()->is_patrocinador)
                  <td> 
                    {!! Form::open(['route' => ['produtos.destroy',$produto->id], 'method' => 'DELETE']) !!}
                      <button class="btn btn-danger" type="submit" title="Remover"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    {!! Form::close() !!}
                    
                  </td>
                  @endif
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $produtos->render() }}
            </div>
          </div>
          <!-- /.box -->
		</div>	
	</div>
</div>
@stop
