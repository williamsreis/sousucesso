$(document).ready(function(){

  $('.data').mask('00/00/0000');
  $('.horario').mask('00:00');

  $(".formRemover").on('submit',function(e){
	e.preventDefault();
	var form = $(this);
	$.confirm({
	    title: 'Alerta',
	    content: 'Você deseja remover?',
	    theme: 'supervan',
	    buttons: {
	        confirm:  {
	        	text: 'Confirmar',
	            btnClass: 'btn-blue',
	            action: function () {

	            	form.unbind('submit').submit();

	            }
	        },
	        cancel: {
	            text: 'Cancelar',
	            btnClass: 'btn-red',

	        }

	    }
    });
   });

});
