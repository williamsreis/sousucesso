﻿$(document).ready(function() {

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
       // $("#uf").val("");
        //$("#ibge").val("");
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#rua").val("...");
                $("#bairro").val("...");
                $("#cidade").val("...");
                //$("#uf").val("...");
                //$("#ibge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#rua").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade+"/"+dados.uf);
                        //$("#uf").val(dados.uf);
                  //      $("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });





    var SPMaskBehavior = function (val) {
	  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
	  onKeyPress: function(val, e, field, options) {
	      field.mask(SPMaskBehavior.apply({}, arguments), options);
	    }
	};
    $('.cpf').mask('000.000.000-00');
    $('.cep').mask('00000-000');
    $('.nascimento').mask('00/00/0000');

	$('.telefone').mask(SPMaskBehavior, spOptions);

    $.fn.select2.defaults.set( "theme", "bootstrap" );

    $(window).bind('scroll', function() {
      if ($(window).scrollTop() > 50) {
        $('.navbar1').addClass('navbar-fixed');
      }else{
        $('.navbar1').removeClass('navbar-fixed');
      }



    });

    $('.slideshow').cycle();

    /*$( ".cidade" ).select2({
        placeholder: 'Cidade/UF',
        width : null,
        ajax: {
            url: "http://localhost/sousucesso/public/cidades",
            dataType: 'json',
            delay: 0,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data) {

                return {
                    results: data
                };
            },
            cache: true
        },
        minimumInputLength: 3
    });*/
    if($("input[name='cadastro']").val() == 'pre'){
         $('.completo').hide();

    }

    $("input[name='cadastro']").on('change', function(event) {
        event.preventDefault();
        if($(this).val() == 'pre'){
            $('.completo').hide();

        }else{
            $('.completo').show();
        }
    });

    $("#formContato").on('submit', function(event) {

	    event.preventDefault();

	    $('alert-success').detach();
	    $('.help-block').detach();

	    var $form = $(this);

	    $(':submit').attr('disabled', true)
	    			.after('<div class="enviando"><i class="fa fa-spinner fa-spin"></i> Enviando...</div>');
	    $.ajax({
	            url: $form.attr('action'),
	            type: 'POST',
	            data: $form.serialize(),
	            success: function(result) {
	                $form[0].reset();
	                $('.help-block').detach();
	                $(':submit').after(successMsg('Obrigado por entrar em contato! Estaremos respondendo o seu e-mail o mais breve possível.'));
	            },
	            error: function(result) {
	              console.log(result.responseJSON);
	              var inputs = Object.keys(result.responseJSON.errors);
	              $.each(result.responseJSON.errors,function(index,value ) {
	                console.log(value);
	                var $el = $("#formContato input[name='"+index+"'], #formContato textarea[name='"+index+"']");
	                $el.after(helpBlock(value));
	                $el.closest('.form-group').addClass('has-error');
	              });
	              $('input[name="'+inputs[0]+'"]').focus();
	            },
	            complete: function(){
	              $(':submit').attr('disabled', false);
	              $('.enviando').detach();

	            }
	        });
	  });
  function helpBlock(help){
    return '<span class="help-block">'+help+'</span>';
  }
  function successMsg(msg){
    return '<br/><br/><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button class="close" aria-hidden="true" data-dismiss="alert" type="button">&times;</button>'+msg+'</div>';
  }
});
