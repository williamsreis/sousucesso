<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cidade;
use App\Models\CaseSucesso;
use App\Http\Requests\FaleConoscoRequest;
use App\Mail\FaleConoscoMail;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cases = CaseSucesso::inRandomOrder()->get();
        return view('site.home',compact('cases'));
    }
    public function casesDeSucesso()
    {
        $cases = CaseSucesso::orderBy('id','DESC')->paginate(20);
        return view('site.case',compact('cases'));
    }

    public function cidades(Request $request){
        $cidades = Cidade::select('cidades.id as id',\DB::raw('concat(cidades.nome,"/",estados.uf) as text'))
                        ->join('estados','cidades.estado','=','estados.id')
                        ->where('cidades.nome','like','%'.$request->q.'%')
                        ->orderBy('cidades.nome','ASC')
                        ->get();

        return response()->json($cidades);
    }

    public function faleConosco(FaleConoscoRequest $request){
        \Mail::to('crescimento@sousucesso.com')->send(new FaleConoscoMail($request->all()));
    }

    public function vantagens(){
        return view('site.vantagens');
    }
    public function quemSomos(){
        return view('site.quem-somos');
    }

    public function conhecaPlano(){
        return view('site.conheca-plano');
    }
}
