<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plano;
use App\Models\Socio;
use App\Http\Requests\SocioRequest;
use App\Mail\AtivarSocioMail;
use App\Mail\SocioAtivadoMail;

class SejaNossoSocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planos = Plano::orderBy('valor','ASC')->get();
        $estadoCivil = [
            'Solteiro' => 'Solteiro',
            'Casado' => 'Casado',
            'Separado' => 'Separado',
            'Divorciado' => 'Divorciado',
            'Viúvo' => 'Viúvo'
        ];
        return view('site.seja-nosso-socio',compact('planos','estadoCivil'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocioRequest $request)
    {
        $dados = $request->all();
        if($dados['nascimento'] != ""){
            list($dia,$mes,$ano) = explode("/",$dados['nascimento']);
            $dados['nascimento'] = $ano."-".$mes."-".$dia;
        }

        $socio = Socio::create($dados);
        \Mail::to($socio->email)->send(new AtivarSocioMail($socio));

        return view('site.cadastro-realizado');
    }

    public function ativarSocio($id)
    {
        $socio = Socio::where('id',$id)->update(['ativo' => 1]);
        \Mail::to("crescimento@sousucesso.com")->send(new SocioAtivadoMail($socio));

        return view('site.socio-ativado');

    }


}
