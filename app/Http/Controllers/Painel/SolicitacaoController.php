<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Solicitacao;
use App\Mail\SolicitacaoMail;
use App\Http\Requests\SolicitacaoRequest;
use Carbon\Carbon;

class SolicitacaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitacoes = Solicitacao::orderBy('id','DESC')->paginate(50);

        return view('painel.solicitacoes.index',compact('solicitacoes'));
    }

    public function create(){
    	return view('painel.solicitacoes.create');
    }

    public function store(SolicitacaoRequest $request)
    {
        
        $dados = $request->all();
        $d = explode("/",$dados['data']);
        $dados['data'] = $d[2]."-".$d[1]."-".$d[0];
        $solicitacao = Solicitacao::create($dados);


        \Mail::to('williamsbsi@gmail.com')->send(new SolicitacaoMail($solicitacao));

        return redirect()->route('solicitacoes.create')->with('message','Sua solicitação foi enviada com sucesso! Aguarde o nosso retorno, obrigado!');


    }
}
