<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\UsuarioRequest;
use App\Http\Controllers\Controller;
use App\Models\Socio;
use App\User;
use App\Mail\SenhaSocioMail;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('patrocinador');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idSocio)
    {
        $socio = Socio::findOrFail($idSocio);
        return view('painel.socios.usuarios.create',compact('socio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioRequest $request, $idSocio)
    {
        
        $senha = substr(md5(rand(1,10000)),0,6);
        
        $user = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($senha),
            'is_socio' => 1
        ];

        if(isset($request->patrocinador)){
            $user['is_patrocinador'] = $request->patrocinador;
        }

        $usuario = User::create($user);

        $socio = Socio::where('id',$idSocio)->first();

        

        $socio->update([
            'user_id' => $usuario->id,
            'url_hinode' => $request->url_hinode,
            'url_sousucesso' => $request->url_sousucesso
        ]);

        \Mail::to($socio->email)->send(new SenhaSocioMail($socio,$senha));

        return redirect()->route('socios.index')->with('message','Usuário cadastrado com sucesso!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idSocio,$id)
    {
        $socio = Socio::findOrFail($idSocio);
        $usuario = User::findOrFail($id);

        return view('painel.socios.usuarios.edit',compact('socio','usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioRequest $request, $idSocio,$id)
    {
        Socio::where('id',$idSocio)->update([
            'url_hinode' => $request->url_hinode,
            'url_sousucesso' => $request->url_sousucesso
        ]);
        $user = [
            'name' => $request->name,
            'email' => $request->email
        ];
        if(isset($request->patrocinador)){
            $user['is_patrocinador'] = $request->patrocinador;
        }
        User::where('id',$id)->update($user);

        return redirect()->route('socios.index')->with('message','Usuário editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
