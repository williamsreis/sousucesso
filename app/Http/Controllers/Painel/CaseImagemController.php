<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CaseSucesso;
use App\Models\CaseImagem;

class CaseImagemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('patrocinador');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idCase)
    {
        $case = CaseSucesso::findOrfail($idCase);
        $imagens = $case->imagens;
        return view('painel.cases.imagens.index',compact('case','imagens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idCase)
    {
        $case = CaseSucesso::findOrfail($idCase);
        return view('painel.cases.imagens.create',compact('case'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        @symlink (storage_path("app"), public_path('app'));
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {

                $nameFile = date("Ymd")."_".str_slug($request->file->getClientOriginalName());
                $extension = $request->file->extension();
                $path = $request->file->storeAs('cases', $nameFile.".".$extension);

                CaseImagem::create([
                    'case_id' => $id,
                    'imagem' => $path
                ]);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
