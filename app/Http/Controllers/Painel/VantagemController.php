<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vantagem;
use App\Http\Requests\VantagensRequest;

class VantagemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('patrocinador');

    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vantagens = Vantagem::orderBy('id','DESC')->paginate(10);
        return view('painel.vantagens.index',compact('vantagens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.vantagens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VantagensRequest $request)
    {
        $vantagem = $request->all();
        @symlink (storage_path("app"), public_path('app'));
        if ($request->hasFile('imagem')) {
            if ($request->file('imagem')->isValid()) {

                $nameFile = date("Ymd")."_".str_slug($request->imagem->getClientOriginalName());
                $extension = $request->imagem->extension();
                $path = $request->imagem->storeAs('vantagens', $nameFile.".".$extension);

                $vantagem['imagem'] = $path;
            }
        }
        Vantagem::create($vantagem);
        return redirect()->route('vantagens.index')->with('message','Cadastro realizado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vantagem = Vantagem::findOrFail($id);
        return view('painel.vantagens.edit',compact('vantagem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VantagensRequest $request, $id)
    {
        $vant = Vantagem::findOrFail($id);
        $vantagem = $request->all();
        @symlink (storage_path("app"), public_path('app'));
        if ($request->hasFile('imagem')) {
            if ($request->file('imagem')->isValid()) {

                $nameFile = date("Ymd")."_".str_slug($request->imagem->getClientOriginalName());
                $extension = $request->imagem->extension();
                $path = $request->imagem->storeAs('vantagens', $nameFile.".".$extension);

                $vantagem['imagem'] = $path;
            }
        }
        $vant->update($vantagem);

        return redirect()->route('vantagens.index')->with('message','Cadastro editado com sucesso!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vantagem::findOrFail($id)->delete();
        
        return redirect()->route('vantagens.index')->with('message','Cadastro apagado com sucesso!');
    }
}
