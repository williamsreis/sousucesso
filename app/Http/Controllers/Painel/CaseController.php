<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CaseSucesso;
use App\Http\Requests\CasesRequest;

class CaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('patrocinador');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cases = CaseSucesso::orderBy('id','DESC')->paginate(20);
        return view('painel.cases.index',compact('cases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.cases.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CasesRequest $request)
    {
        CaseSucesso::create($request->all());
        return redirect()->route('cases.index')->with('message','Cadastro realizado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case = CaseSucesso::findOrFail($id);
        return view('painel.cases.edit',compact('case'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CasesRequest $request, $id)
    {
        $case = CaseSucesso::findOrFail($id);
        $case->update($request->all());

        return redirect()->route('cases.index')->with('message','Cadastro editado com sucesso!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CaseSucesso::findOrFail($id)->delete();
        
        return redirect()->route('cases.index')->with('message','Cadastro apagado com sucesso!');
    }
}
