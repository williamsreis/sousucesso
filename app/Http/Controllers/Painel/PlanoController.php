<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plano;
use App\Http\Requests\PlanosRequest;

class PlanoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('patrocinador');

    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planos = Plano::orderBy('id','DESC')->paginate(10);
        return view('painel.planos.index',compact('planos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.planos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanosRequest $request)
    {
        $plano = $request->all();
        $plano['valor'] = str_replace("R$ ","",$plano['valor']);
        $plano['valor'] = str_replace(".","",$plano['valor']);
        $plano['valor'] = str_replace(",",".",$plano['valor']);
        Plano::create($plano);
        return redirect()->route('planos.index')->with('message','Cadastro realizado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case = Plano::findOrFail($id);
        return view('painel.planos.edit',compact('case'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlanosRequest $request, $id)
    {
        $case = Plano::findOrFail($id);
        $plano = $request->all();
        $plano['valor'] = str_replace("R$ ","",$plano['valor']);
        $plano['valor'] = str_replace(".","",$plano['valor']);
        $plano['valor'] = str_replace(",",".",$plano['valor']);
        $case->update($plano);

        return redirect()->route('planos.index')->with('message','Cadastro editado com sucesso!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Plano::findOrFail($id)->delete();
        
        return redirect()->route('planos.index')->with('message','Cadastro apagado com sucesso!');
    }
}
