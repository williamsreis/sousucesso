<?php

namespace App\Http\Middleware;

use Closure;

class IsSocio
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->is_socio != 1) {
            return redirect('virtual-office/login');
        }
        return $next($request);
    }
}
