<?php

namespace App\Http\Middleware;

use Closure;

class IsPatrocinador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->is_patrocinador != 1) {
            return redirect('painel/login');
        }
        return $next($request);
    }
}
