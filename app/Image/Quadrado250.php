<?php

namespace App\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Quadrado250 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(200);
    }
}