<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model
{
    public $timestamps = false;
    protected $fillable = ['nome','valor','descricao'];
 
    public function getValorRealAttribute(){
    	return "R$ ".number_format($this->valor,2, ',', '.');
    }
}
