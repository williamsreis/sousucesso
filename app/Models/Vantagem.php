<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vantagem extends Model
{
    public $timestamps = true;
    protected $fillable = ['titulo','url','descricao','imagem'];
    protected $table = 'vantagens';
    
}
