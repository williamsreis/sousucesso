<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaseImagem extends Model
{
	public $table = 'case_imagens';
    public $timestamps = false;
    protected $fillable = ['case_id','imagem'];
}
