<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    public $table = 'solicitacoes';
    public $timestamps = false;
    protected $fillable = ['socio_id','qtd_pessoas','data','horario','local_definido','obs'];

    public function socio(){
    	return $this->belongsTo('App\Models\Socio')->withDefault();
    }

    public function getLocalDefinidoAttribute($value){
    	return ($value == 1)?'SIM':'NÃO';
    }


    protected $dates = [
        'data'
    ];


}
