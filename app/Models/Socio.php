<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    protected $fillable = [
        'nome','email','telefone','cidade','obs','patrocinador','plano_id','ativo','user_id','url_hinode','url_sousucesso',
        'cpf',
        'rg',
        'nascimento',
        'estado_civil',
        'dependentes',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'complemento',
    ];

    protected $dates = ['nascimento'];

    public function user(){
    	return $this->belongsTo('App\User')->withDefault();
    }

    public function cidade2(){
    	return $this->belongsTo('App\Models\Cidade','cidade','id')->withDefault();
    }
}
