<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaseSucesso extends Model
{
    public $table = 'cases';
    public $timestamps = false;
    protected $fillable = ['nome','empresa','descricao'];


	public function imagens(){
		return $this->hasMany("App\Models\CaseImagem","case_id");
	}
	public function imagem(){
		return $this->hasOne("App\Models\CaseImagem","case_id");
	}



}
