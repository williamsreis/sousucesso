<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AtivarSocioMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $socio;
    public function __construct($socio)
    {
        $this->socio = $socio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('crescimento@sousucesso.com','Sou Sucesso')
                    ->view('emails.ativar-socio')
                    ->subject("Marketing em Rede");
    }
}
