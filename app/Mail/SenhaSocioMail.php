<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SenhaSocioMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $socio;
    public $senha;

    public function __construct($socio,$senha)
    {
        $this->socio = $socio;
        $this->senha = $senha;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('crescimento@sousucesso.com','Sou Sucesso')
                    ->view('emails.senha-socio')
                    ->subject("Sua senha - Marketing em Rede");
    }
}
