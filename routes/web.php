<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "Site\HomeController@index");
//Route::get('/vantagens', "Site\HomeController@vantagens");
Route::get('/vantagens', "Site\HomeController@quemSomos");
Route::get('/conheca-o-plano', "Site\HomeController@conhecaPlano");
Route::post('/fale-conosco', "Site\HomeController@faleConosco")->name('faleconosco');

Route::get('/cases-de-sucesso', "Site\HomeController@casesDeSucesso");
Route::get('/seja-nosso-socio', "Site\SejaNossoSocioController@index");
Route::post('/seja-nosso-socio/salvar', "Site\SejaNossoSocioController@store")->name('seja-nosso-socio');
Route::get('/quero-entrar-no-marketing-em-rede/{id}', "Site\SejaNossoSocioController@ativarSocio");

Route::get("/cidades","Site\HomeController@cidades" );

Auth::routes();

Route::namespace('Painel')->group(function () {
	Route::prefix('painel')->group(function () {
		Route::get('home', 'HomeController@index')->name('home');
		Route::resource('cases', 'CaseController');
		Route::resource('cases/{caseID}/imagens', 'CaseImagemController');
		Route::resource('planos', 'PlanoController');
		Route::resource('vantagens', 'VantagemController');
		Route::resource('produtos', 'ProdutoController');
		Route::resource('socios', 'SocioController');
		Route::resource('socios/{socioID}/usuario', 'UsuarioController');

		Route::resource('solicitacoes', 'SolicitacaoController');

	});
});

Route::namespace('VirtualOffice')->group(function () {
	Route::prefix('virtual-office')->group(function () {
		Route::get('login','LoginController@index');
		Route::post('login','LoginController@login');
	});
});

